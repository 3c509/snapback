# Snapback #

Utility to automate data protection of AWS EC2 instances running Linux via EBS Snapshots.

### Overview ###

snapback is designed to allow the user to specify the maximum number of recovery points / snapshots that are kept via the -m <number> argument.  Once the number of recovery points / snapshots have been met the oldest snapshot will be deleted.  

The proper way to use this script is by assigning a role to the EC2 instance which provides it the ability to create snapshots.  AmazonEC2FullAccess is recommended.

The script was written using bash.  It is intended to be ran via cron to automate the data protection.  

Errors are logged via syslog and STDERR.  The script will exit with an error code of 0 upon success.

Snapshots are named using the EC2 Instance ID followed by an index number which increments as snapshots are created.


### Usage ###

 snapback.sh -m <number>

* Sample Output


```
 [root@ip-10-0-1-225 bin]# ./snapback.sh -m 2
 Summary: 2 out of 2 snapshots currently exist
 Creating snapshot: i-0e6d9c5b3e6f45db8-9 from volume vol-ad6c1e7d
 {
     "Description": "i-0e6d9c5b3e6f45db8-9",
     "Encrypted": false,
     "VolumeId": "vol-ad6c1e7d",
     "State": "pending",
     "VolumeSize": 8,
     "Progress": "",
     "StartTime": "2016-06-06T21:46:08.000Z",
     "SnapshotId": "snap-dea7d3c6",
     "OwnerId": "631208945683"
 }
 Verifying snapshot: i-0e6d9c5b3e6f45db8-9
 .......
 Completed snapshot: i-0e6d9c5b3e6f45db8-9
 Deleted old snapshot: i-0e6d9c5b3e6f45db8-7
 [root@ip-10-0-1-225 bin]# 
```