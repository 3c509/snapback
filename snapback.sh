#!/bin/sh
#
################################
# Overview and Description
################################
#
# - utility to automate snapshots of ec2 instance
# - script is controlled via CLI arguments
# - the -m argument is manditory and controls the maximum number of snapshots to keep
# - once the maximum number of snapshots have been reached the oldes snapshot will be removed
#
# - designed to be hands off and used via scheduled cron job
# - script tries its best to check for error conditions
# - snapshots start at index of 1 when no snapshots exist
# - snapshot index always increments forward
# - oldest snapshot is removed after new snapshot is verified
#
# *** Usage - Create Snapshot ***
#
#
# snapback.sh -m <number>
#
#
# *** Example Output: Create Snapshot with a total of 2 Snapshots ***
#
# [root@ip-10-0-1-225 bin]# ./snapback.sh -m 2
# Summary: 2 out of 2 snapshots currently exist
# Creating snapshot: i-0e6d9c5b3e6f45db8-9 from volume vol-ad6c1e7d
# {
#     "Description": "i-0e6d9c5b3e6f45db8-9",
#     "Encrypted": false,
#     "VolumeId": "vol-ad6c1e7d",
#     "State": "pending",
#     "VolumeSize": 8,
#     "Progress": "",
#     "StartTime": "2016-06-06T21:46:08.000Z",
#     "SnapshotId": "snap-dea7d3c6",
#     "OwnerId": "631208945683"
# }
# Verifying snapshot: i-0e6d9c5b3e6f45db8-9
# .......
# Completed snapshot: i-0e6d9c5b3e6f45db8-9
# Deleted old snapshot: i-0e6d9c5b3e6f45db8-7
#
#
#
################################
#
# Change Log
#
################################
#
# Date (Y-M-d)  - Revision      - Author, Comment
# ----------------------------------------
#
# 2016-06-06    - 0.1           - Alex Theodore, Initial Version, Dev/Test
#
#
#
#
#
#
#
#
#
#
#
############[ Do Not Modify Script ]################################







############[ Script Functions ]###################################

precheck() {
  if [ ! -f /usr/bin/aws ]; then
    echo "ERROR: $0 - aws cli not found, aborting" >&2
    exit 1
  fi

}

mksnap() {
  aws ec2 create-snapshot --volume-id ${volume} --description "${instance}-${new_idx}"
}

verifysnap() {
  newinfo=/tmp/newinfo-$$.out

  aws ec2 describe-snapshots --owner-ids ${ownerid} --filters Name=description,Values="${instance}-${new_idx}" > ${newinfo}
  complete_pct=`grep Progress ${newinfo} | awk '{print $2}' | sed 's/%",//g' | sed 's/"//g'`
  
  while [ ${complete_pct} -lt 100 ]; do
    sleep 10s
    echo -ne "."
    verifysnap
  done

  rm -f ${newinfo}

}

deletesnap() {
  newinfo=/tmp/newinfo-$$.out

  # determine snapshot id
  aws ec2 describe-snapshots --owner-ids ${ownerid} --filters Name=description,Values="${instance}-${oldest_snap_idx}" > ${newinfo}
  old_snap_id=`grep SnapshotId ${newinfo} | awk '{print $2}' | sed 's/",//g' | sed 's/"//g'`

  aws ec2 delete-snapshot --snapshot-id ${old_snap_id}

  rm -f ${newinfo}
}

cleanup() {
  rm -f ${snapinfo} ${snaplist} ${newinfo} ${instinfo}
}

helpfunc() {
  echo "Usage: $0 -m <number>"
  echo ""
  echo "Creates snapshot of an ec2 instance."
  echo "-m # 	specifies maximum number of snapshots to keep."
  echo "	when script reaches maximum it will remove the oldest snapshot."
  echo ""
  echo ""
  echo "Assumptions:"
  echo "  - snapshot description is in the format of ec2 InstanceID-index"
  echo "  - snapshots start at index of 1 when no snapshots exist"
  echo "  - snapshot index always increments forward"
  echo "  - oldest snapshot is removed after new snapshot is verified if maximum snapshot value is reached"
  echo ""
}

############[ Main Section of Script ]###################################

# Parse CLI arguments for -m flag
#
while getopts ":m:" opt; do
  case $opt in
    m)
      max_num_snaps=$OPTARG
      pattern='^[0-9]+$'
      if ! [[ ${max_num_snaps} =~ ${pattern} ]]; then
        echo "Invalid argument: -m $OPTARG  ** argument must be numeric **" >&2
        exit 1
      fi
      ;;
    \?)
      helpfunc
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Verify that -m was called otherwise show help and exit 
#
if [ -z ${max_num_snaps} ]; then
  helpfunc
  exit 1
fi
  
# Double check that only 2 arguments were used 
#
if [ $# -ne 2 ]; then
  helpfunc
  exit 1
fi 

precheck

echo "----------------------- "
echo "Started at $(date)"

# temporary workfiles
snapinfo="/tmp/snapinfo-$$.out"
snaplist="/tmp/snaplist-$$.out"
instinfo="/tmp/instinfo-$$.out"

# Identify ec2 instance id
#
instance=`curl http://169.254.169.254/latest/meta-data/instance-id 2>/dev/null`
if [ ! ${#instance} -gt 0 ]; then
  echo "ERROR: $0 - unable to determine instance id, aborting" >&2
  exit 1
fi

# Identify volume id of ec2 instance
#
aws ec2 describe-instances --instance-ids ${instance} > ${instinfo}
volume=`grep VolumeId ${instinfo} | awk '{print $2}' | sed 's/"//g' | sed 's/,//g'`

if [ ! ${#volume} -gt 0 ]; then
  echo "ERROR: $0 - unable to determine volume id, aborting" >&2
  exit 1
fi

# Identify OwnerId
#
ownerid=`grep OwnerId ${instinfo} | head -1 | awk '{print $2}' | awk -F \" '{print $2}'`

if [ ! ${#ownerid} -gt 0 ]; then
  echo "ERROR: $0 - unable to determine OwnerID, aborting" >&2
  exit 1
fi



# Capture snapshot informaiton for parsing
aws ec2 describe-snapshots --owner-ids ${ownerid} > ${snapinfo}

# Build list of existing snapshots
grep ${instance}-[0-9]. ${snapinfo} | awk '{print $2}' | awk -F\" '{print $2}' > ${snaplist}

# Determine total number of snapshots that exist
total_snaps=`wc -l < ${snaplist}`

# Set New snapshot number index value
if [ ${total_snaps} -eq 0 ]; then
  # no snapshots found, starting index at 1
  new_idx=1
else
  # Identify oldest snapshot number
  oldest_snap_idx=`awk -F - '{print $3}' < ${snaplist} | sort -g | head -1`

  # Identify newest snapshot number
  newest_snap_idx=`awk -F - '{print $3}' < ${snaplist} | sort -gr | head -1`

  # increment index by one from latest snapshot
  new_idx=$((${newest_snap_idx} + 1))
fi

# create snapshot, verify snapshot --  max # of snapshots not yet reached
#
if [ ${total_snaps} -lt ${max_num_snaps} ]; then
  echo "Summary: ${total_snaps} out of ${max_num_snaps} snapshots currently exist"
  echo "Creating snapshot: ${instance}-${new_idx} from volume ${volume}"
  mksnap
  echo "Verifying snapshot: ${instance}-${new_idx}"
  verifysnap
  echo ""
  echo "Completed snapshot: ${instance}-${new_idx}"

# create snapshot, verify snapshot -- max # of snapshots reached
#    delete oldest snapshot after verification of new snapshot creation
#
elif [ ${total_snaps} -eq ${max_num_snaps} -o ${total_snaps} -gt ${max_num_snaps} ]; then
  echo "Summary: ${total_snaps} out of ${max_num_snaps} snapshots currently exist"
  echo "Creating snapshot: ${instance}-${new_idx} from volume ${volume}"
  mksnap
  echo "Verifying snapshot: ${instance}-${new_idx}"
  verifysnap
  echo ""
  echo "Completed snapshot: ${instance}-${new_idx}"
  deletesnap
  echo "Deleted old snapshot: ${instance}-${oldest_snap_idx}"

# this condition should never get triggered
#
else
  echo "Unknown condition" >&2

fi

cleanup
echo "Finished at $(date)"
echo "----------------------- "
exit 0
